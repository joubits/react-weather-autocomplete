import React, { Component } from 'react'
import '../styles/search.css';

export default class Search extends Component {
    onSearch(e) {
        this.props.onchangecity(e.target.value);
    }
    render() {
        return (
            <div className="row justify-content-md-center">
                <div className="col-sm-4 text-center">
                    <input type="text" className="form-control" name="city" autoComplete="off" 
                        placeholder="City" onChange={ (e) => this.onSearch(e) } value={this.props.searchvalue}/>
                </div>
            </div>
        )
    }
}
