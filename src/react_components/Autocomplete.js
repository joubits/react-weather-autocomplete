import '../styles/autocomplete.css' ;
import React, { Component } from 'react';

export default class Autocomplete extends Component {
    onCitySelected(cityName, countryName) {
        this.props.onclicked(cityName, countryName);
    }
    render() {
        return (
            <div class="row justify-content-md-center">
                <div className="col-sm-4 match-list">
                    {
                        this.props.citylist && this.props.searchvalue ? this.props.citylist.map( (singleCity) =>
                        <div key={singleCity.lat + singleCity.lng}
                            onClick={() => this.onCitySelected(singleCity.name, singleCity.country)} >
                                {singleCity.name}, {singleCity.country}
                        </div>
                        ) : null 
                    }
                </div>
            </div>
        )
    }
}


