import React, { Component } from 'react'

export default class Weather extends Component {
    render() {
        return (
            <div className="row justify-content-md-center text-light">
                <div className="cards col-sm-4" style={{ display: this.props.searchvalue === ''|| this.props.weatherInfo.city === '' ? 'none' : 'block' }}>
                    <h1>{this.props.weatherInfo.city}, {this.props.weatherInfo.country}</h1>
                    <h5 className="py-4">
                        <i className={`wi ${this.props.weatherInfo.icon} display-1`}></i>
                    </h5>
                    <h1 className="py-2">{this.props.weatherInfo.temp}&deg;</h1>
                    {/* show minmax Values */}
                    { minMaxTemp(this.props.weatherInfo.temp_min, this.props.weatherInfo.temp_max) }
                    <h4 className="py-3">{this.props.weatherInfo.description}</h4>
                </div>
            </div>
        )
    }
}

function minMaxTemp(min, max) {
    return (
        <h3>
            <span className="px-4">{ min }&deg;</span>
            <span className="px-4">{ max }&deg;</span>
        </h3>
    );
}
