import React, { Component } from 'react'

import Weather from './react_components/Weather';
import Search from './react_components/Search';
import cities from 'cities.json';
import Autocomplete from './react_components/Autocomplete'

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'weather-icons/css/weather-icons.css';

const API_KEY = '21dbeaa125072ca72dc0501784201933';
// list of all cities in the world from  cities.json package
const allCities = cities;

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      city: '',
      country: '',
      temp: 0,
      temp_max: 0,
      temp_min: 0,
      description: '',
      icon: '',
      searchValue: '',
      citiesToSearch: []
    }

    this.weatherIcon = {
      Thunderstorm: 'wi-thunderstorm',
      Drizzle: 'wi-sleet',
      Rain: 'wi-storm-showers',
      Snow: 'wi-snow',
      Atmosphere: 'wi-fog',
      Clear: 'wi-day-sunny',
      Clouds: 'wi-day-fog'
    }
  }

  calcCelsius(temp) {
    if(!temp) return 0;
    return Math.round(temp - 273.15);
  }

  getWeatherIcon(icons, rangeId) {
    switch (true) {
      case rangeId >=200 && rangeId <=232:
        this.setState({ icon:this.weatherIcon.Thunderstorm })
        break;
      case rangeId >=300 && rangeId <=321:
        this.setState({ icon:this.weatherIcon.Drizzle })
        break;
      case rangeId >=500 && rangeId <=531:
        this.setState({ icon:this.weatherIcon.Rain })
        break;
      case rangeId >=600 && rangeId <=622:
        this.setState({ icon:this.weatherIcon.Snow })
        break;
      case rangeId >=701 && rangeId <=781:
        this.setState({ icon:this.weatherIcon.Atmosphere })
        break;
      case rangeId===800:
        this.setState({ icon:this.weatherIcon.Clear })
        break;
      case rangeId >=801 && rangeId <=801:
        this.setState({ icon:this.weatherIcon.Clouds })
        break;
      default:
        this.setState({ icon:this.weatherIcon.Clouds })
    }
  }

  onChangeCity = (cityName) => {
    const citiesFiltered = [];
    if(cityName.length === 0){
      this.setState({ citiesToSearch: [] });
    }
    this.setState({ searchValue: cityName });
    let cityInput = this.state.searchValue
    if(cityInput.length > 1){
      for (const city of allCities) {
        if(String(city.name).toLowerCase().startsWith(cityName.toLowerCase())){
          citiesFiltered.push(city);
        }
      }
      citiesFiltered.sort(this.compare);
      this.setState({ citiesToSearch: citiesFiltered.slice(0,10) }, () => {
      });
    }
  }
  // compare function
  compare = ( a, b ) => {
    if ( a.name < b.name ){
      return -1;
    }
    if ( a.name > b.name ){
      return 1;
    }
    return 0;
  }

  getWeather = async (city, country) => {
    let response = '';
    if(city!==null) {
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}`;

      const apiCall = await fetch(url);
      response = await apiCall.json();
    }
    console.log(response);
    this.setState({
      city: response.name,
      country: response.sys.country,
      temp: this.calcCelsius(response.main.temp),
      temp_max: this.calcCelsius(response.main.temp_max),
      temp_min: this.calcCelsius(response.main.temp_min),
      description: response.weather[0].description
    })
    this.getWeatherIcon(this.weatherIcon, response.weather[0].id);
  }

  onCitySelected = (cityName, countryCode) => {
    console.log(cityName, countryCode);
    this.setState({ citiesToSearch: [], searchValue: cityName }); 
    this.getWeather(cityName, countryCode);
  }
  
  render() {
    return (
      <div className="App container">
        <Search onchangecity={ this.onChangeCity } searchvalue={this.state.searchValue}/>
        <Autocomplete citylist={this.state.citiesToSearch} 
                      onclicked={this.onCitySelected} 
                      searchvalue={this.state.searchValue} />
        <Weather weatherInfo={this.state} searchvalue={this.state.searchValue} />
      </div>
    )
  }
}

